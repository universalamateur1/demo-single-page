# Dynamiv Enviroment for Review Apps with a static HTML Page (Hugo)

- for the MR review app we copy the files to the folder /public/$FEATUREBRANCH/$COMMIT_REF and sets a cleanup job for review stop which deletes this folder
- for the staging from default we copy to /public/staging/ and clean up with Staging stop
- For default branch and tag we copy only to /public/
- We use a Markdown Linter as test job in the CI and secret detection

## to come

- Needs will be used
- An image will be created on Commit with Nginx and the public folder.
- A global variable for manual Pipeline start
- With this all cicd cert things shall be covered, check the git one if that is the same.
