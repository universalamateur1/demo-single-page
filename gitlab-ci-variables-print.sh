#!/bin/bash

# Output HTML file
OUTPUT_FILE="ci_cd_variables.html"

# Generate HTML header
echo '<html>
<head>
  <title>GitLab CI/CD Variables</title>
  <style>
    table {
      border-collapse: collapse;
      width: 80%;
      margin: 20px;
    }
    th, td {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    th {
      background-color: #f2f2f2;
    }
  </style>
</head>
<body>
  <h1>GitLab CI/CD Variables</h1>
  <table>
    <tr>
      <th>Name</th>
      <th>Value</th>
      <th>Description</th>
    </tr>' > "$OUTPUT_FILE"

# Generate HTML rows for predefined CI/CD variables
# Generate HTML rows for predefined CI/CD variables
echo "<tr><td>CI_COMMIT_REF_NAME</td><td>$CI_COMMIT_REF_NAME</td><td>Branch or tag name for which project is built</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_REF_SLUG</td><td>$CI_COMMIT_REF_SLUG</td><td>Ref name as a valid filesystem name</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_SHA</td><td>$CI_COMMIT_SHA</td><td>Commit SHA for which project is built</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_SHORT_SHA</td><td>$CI_COMMIT_SHORT_SHA</td><td>Shortened commit SHA</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_MESSAGE</td><td>$CI_COMMIT_MESSAGE</td><td>Commit message</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_TITLE</td><td>$CI_COMMIT_TITLE</td><td>Commit title</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_DESCRIPTION</td><td>$CI_COMMIT_DESCRIPTION</td><td>Commit description</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_REF_PROTECTED</td><td>$CI_COMMIT_REF_PROTECTED</td><td>Whether the branch or tag is protected</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PIPELINE_ID</td><td>$CI_PIPELINE_ID</td><td>Unique ID of the current pipeline</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PIPELINE_IID</td><td>$CI_PIPELINE_IID</td><td>Internal ID of the current pipeline</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PIPELINE_SOURCE</td><td>$CI_PIPELINE_SOURCE</td><td>Source of the pipeline</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PIPELINE_CREATED_AT</td><td>$CI_PIPELINE_CREATED_AT</td><td>Timestamp when the pipeline was created</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_ID</td><td>$CI_PROJECT_ID</td><td>ID of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_NAME</td><td>$CI_PROJECT_NAME</td><td>Name of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_TITLE</td><td>$CI_PROJECT_TITLE</td><td>Title of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_PATH</td><td>$CI_PROJECT_PATH</td><td>Path of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_PATH_SLUG</td><td>$CI_PROJECT_PATH_SLUG</td><td>Path of the project as a valid URL path segment</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_NAMESPACE</td><td>$CI_PROJECT_NAMESPACE</td><td>Namespace (group) of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_URL</td><td>$CI_PROJECT_URL</td><td>URL of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PROJECT_VISIBILITY</td><td>$CI_PROJECT_VISIBILITY</td><td>Visibility of the project</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_REGISTRY</td><td>$CI_REGISTRY</td><td>Registry URL, only for images</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_REGISTRY_IMAGE</td><td>$CI_REGISTRY_IMAGE</td><td>Image name, only for images</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_REGISTRY_USER</td><td>$CI_REGISTRY_USER</td><td>Username to login to the registry, only for images</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_REGISTRY_PASSWORD</td><td>$CI_REGISTRY_PASSWORD</td><td>Password to login to the registry, only for images</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_COMMIT_REF_PROTECTED</td><td>$CI_COMMIT_REF_PROTECTED</td><td>Whether the branch or tag is protected</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PAGES_DOMAIN</td><td>$CI_PAGES_DOMAIN</td><td>Pages Domain</td></tr>" >> "$OUTPUT_FILE"
echo "<tr><td>CI_PAGES_URL</td><td>$CI_PAGES_URL</td><td>Pages URL</td></tr>" >> "$OUTPUT_FILE"
CI_PAGES_URL

# Add more variables as needed

# Close HTML tags
echo '</table>
</body>
</html>' >> "$OUTPUT_FILE"

echo "HTML file generated: $OUTPUT_FILE"
